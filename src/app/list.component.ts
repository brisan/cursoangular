import { Component } from '@angular/core';

@Component({
  selector: 'my-list',
   template: `
<myapp *ngFor="let obj of objeto" [myarray]="obj"></myapp>
  `
})
export class MyListComponent {
  objeto: Object[];
  
  constructor() {
    this.objeto = [
      new Myapp("Vencer el mal", "El mal se vence alciendo el bien"),
      new Myapp("Secreto del amor", "Sólo se puede amar a otros, cuando se tiene amor propio"),
      new Myapp("El perdón", "Mira los niños, ellos olvidan las ofensas porque su mente solo retiene lo importante, que les permite crecer y aprender")
    ];
  }
}