import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent, MyListComponent, MyappComponent } from './app.component';
import { MyListComponent } from './list.component';

@NgModule({
  declarations: [
    AppComponent,
    MyListComponent,
    MyappComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
