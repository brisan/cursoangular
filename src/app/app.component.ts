import { Component, Input } from '@angular/core';

class Myapp {
  public titulo: string;
  public mensaje: string;
  public hide: boolean;
  
  constructor(titulo: string, mensaje: string){
    this.titulo = titulo;
    this.mensaje = mensaje;
    this.hide = true;
  }
  
  toggle(){
    this.hide = !this.hide;
  }
}


@Component({
  selector: 'myapp',
  templateUrl: "app.component.html"
})
export class MyappComponent {
  @Input("myarray") myapp: Myapp;
}




@Component({
  selector: 'app-root',
  template: '<my-list></my-list>',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
